import { ActivatedRoute, Params } from '@angular/router';
import { Game } from 'src/app/models/game-model';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-game-details',
  templateUrl: './game-details.component.html',
  styleUrls: ['./game-details.component.scss']
})
export class GameDetailsComponent implements OnInit, OnDestroy {
  gameRating = 0;
  gameId: string;
  game: Game;
  routeSub: Subscription;
  gameSub: Subscription;

  constructor(
    private ActivatedRoute: ActivatedRoute,
    private httpService: HttpService,

  ) { }

  ngOnInit(): void {
    this.routeSub = this.ActivatedRoute.params.subscribe((params: Params) =>
    {
      this.gameId = params ['id'];
      this.getGameDetails(this.gameId);
    })
  }
  getGameDetails(id: string) {
    this.gameSub = this.httpService
      .getGameDetails(id)
      .subscribe((gameResp: Game) => {
        this.game = gameResp;

        setTimeout(() => {
          this.gameRating= this.game.metacritic
        }, 1000);
      })
  }

  //with gauge implmentation
  getColor(value: number): string {
    if (value > 75) {
      //green
      return '#5ee432';
    }else if (value > 50) {
      //orange
      return 'fffa50';
    }else if (value > 30) {
      //yellow
      return '#f7aa38';
    }else {
      //red
      return '#ef4655';
    }
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    if (this.gameSub) {
      this.gameSub.unsubscribe();
    }

    if(this.routeSub) {
      this.routeSub.unsubscribe();
    }
  }

}
