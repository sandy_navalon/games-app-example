import { Observable } from 'rxjs';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class HttpHeadersInterceptor implements HttpInterceptor{
  constructor() {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler

  ): Observable<HttpEvent<any>> {

    //entra API
    req = req.clone({
      setHeaders: {
        'x-rapidapi-key': 'ece34ac85amsh65a188b51783558p1c399fjsn099dff70ebfc',
        'x-rapidapi-host': 'rawg-video-games-database.p.rapidapi.com',
      },
      setParams:{
        key: 'c7168eb6afc3469d8085c263e2c5e294',
      }
    });
    return next.handle(req);
  }

}
