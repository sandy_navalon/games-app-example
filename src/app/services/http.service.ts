import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, map, Observable } from 'rxjs';
import { environment as env} from 'src/environments/environment';
import { APIResponse, Game } from '../models/game-model';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  //inject http client, communication with backend
  constructor(private httpClient: HttpClient) { }

  getGameList (
    ordering: string,
    search?: string,
  ): Observable <APIResponse<Game>> {
    let params = new HttpParams().set('ordering', ordering);

    if(search) {
      params = new HttpParams().set('ordering', ordering).set('search', search);
    }

    return this.httpClient.get<APIResponse<Game>>(`${env.BASE_URL}/games`, {
      params: params,
    })
  }

  getGameDetails (
    id: string
    ): Observable<Game> {
      const gameInfoRequest = this.httpClient.get(`${env.BASE_URL}/games/${id}`);
      const gameTrailerRequest = this.httpClient.get(`${env.BASE_URL}/games/${id}/movies`);
      const gameShootsRequest = this.httpClient.get(`${env.BASE_URL}/games/${id}/screenshots`);

      //combina todos los get en uno
      return forkJoin ({
        gameInfoRequest,
        gameShootsRequest,
        gameTrailerRequest,
      }).pipe (
        map((res: any) => {
          return {
            ...res['gameInfoRequest'],
            screenshots: res['gameShootsRequest']?.results,
            trailers: res['gameTrailerRequest']?.results,
          }
        })
      )
  }
}
